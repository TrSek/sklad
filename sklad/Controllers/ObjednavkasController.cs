﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sklad.Data;
using sklad.Models;

namespace sklad.Controllers
{
    public class ObjednavkasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ObjednavkasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Objednavkas
        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Objednavka.ToListAsync());
        }

        // GET: Objednavkas/ShowSearchObjednavkaForm
        [Authorize]
        public IActionResult ShowSearchObjednavkaForm()
        {
            return View("ShowSearchObjednavkaForm");
        }

        // POST: Objednavkas/ShowSearchObjednavkaForm
        [Authorize]
        public async Task<IActionResult> ShowSearchObjednavkaResults(string SearchPhrase)
        {
            return View("Index", await _context.Objednavka.Where
                (obj => obj.jmeno.Contains(SearchPhrase) || obj.adresa.Contains(SearchPhrase))
                .ToListAsync());
        }

        // GET: Objednavkas/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objednavka = await _context.Objednavka
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objednavka == null)
            {
                return NotFound();
            }

            await objednavka.UpdateProdukt(_context);
            return View(objednavka);
        }

        // GET: Objednavkas/Create
        [Authorize]
        public async Task<IActionResult> Create()
        {
            Objednavka objTemp = new Objednavka();
            await objTemp.UpdateVsetkyProdukty(_context);

            return View(objTemp);
        }

        // POST: Objednavkas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,jmeno,adresa,datum")] Objednavka objednavka, string Id, string DeleteProdukt, string AddProdukt, string Id_produkt)
        {
            if (ModelState.IsValid)
            {
                objednavka.Id = int.Parse(Id);
                if (objednavka.Id == 0)
                {
                    _context.Add(objednavka);
                    await _context.SaveChangesAsync();
                }

                if (!string.IsNullOrEmpty(DeleteProdukt))
                {
                    await DeleteProduktInObjednavka(objednavka.Id, int.Parse(DeleteProdukt));
                }

                if (!string.IsNullOrEmpty(AddProdukt))
                {
                    await AddProduktInObjednavka(objednavka.Id, int.Parse(Id_produkt));
                }

                if (!string.IsNullOrEmpty(DeleteProdukt) || !string.IsNullOrEmpty(AddProdukt))
                {
                    await objednavka.UpdateProdukt(_context);
                    await objednavka.UpdateVsetkyProdukty(_context);
                    return View(objednavka);
                }

                await objednavka.UpdateProdukt(_context);
                _context.Update(objednavka);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            await objednavka.UpdateProdukt(_context);
            await objednavka.UpdateVsetkyProdukty(_context);
            return View(objednavka);
        }

        // GET: Objednavkas/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objednavka = await _context.Objednavka.FindAsync(id);
            if (objednavka == null)
            {
                return NotFound();
            }
            await objednavka.UpdateProdukt(_context);
            await objednavka.UpdateVsetkyProdukty(_context);
            return View(objednavka);
        }

        // POST: Objednavkas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,jmeno,adresa,datum")] Objednavka objednavka, string DeleteProdukt, string AddProdukt, string Id_produkt)
        {
            if (id != objednavka.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(DeleteProdukt))
                    {
                        await DeleteProduktInObjednavka(objednavka.Id, int.Parse(DeleteProdukt));
                    }

                    if (!string.IsNullOrEmpty(AddProdukt))
                    {
                        await AddProduktInObjednavka(objednavka.Id, int.Parse(Id_produkt));
                    }

                    await objednavka.UpdateProdukt(_context);
                    _context.Update(objednavka);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ObjednavkaExists(objednavka.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                if (!string.IsNullOrEmpty(DeleteProdukt) || !string.IsNullOrEmpty(AddProdukt))
                {
                    await objednavka.UpdateVsetkyProdukty(_context);
                    return View(objednavka);
                }

                return RedirectToAction(nameof(Index));
            }
            await objednavka.UpdateProdukt(_context);
            await objednavka.UpdateVsetkyProdukty(_context);
            return View(objednavka);
        }

        // GET: Objednavkas/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objednavka = await _context.Objednavka
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objednavka == null)
            {
                return NotFound();
            }

            await objednavka.UpdateProdukt(_context);
            return View(objednavka);
        }

        // POST: Objednavkas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var objednavka = await _context.Objednavka.FindAsync(id);
            _context.Objednavka.Remove(objednavka);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // Delete partial product
        public async Task DeleteProduktInObjednavka(int id, int Id_product)
        {
            // najdeme id
            List<ObjednavkaProdukt> product1 = _context.ObjednavkaProdukt.Where(objp => objp.Id_objednavka == id && objp.Id_produkt == Id_product).ToList();

            if (product1.Count < 1)
                throw new Exception("Chyba pri mazani");

            var product = await _context.ObjednavkaProdukt.FindAsync(product1[0].Id);
            _context.ObjednavkaProdukt.Remove(product);
            await _context.SaveChangesAsync();
        }

        // Add partial product
        public async Task AddProduktInObjednavka(int id, int Id_product)
        {
            ObjednavkaProdukt product = new ObjednavkaProdukt();
            product.Id_objednavka = id;
            product.Id_produkt = Id_product;

            _context.ObjednavkaProdukt.Add(product);
            await _context.SaveChangesAsync();
        }

        private bool ObjednavkaExists(int id)
        {
            return _context.Objednavka.Any(e => e.Id == id);
        }
    }
}
