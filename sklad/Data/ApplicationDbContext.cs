﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using sklad.Models;

namespace sklad.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<sklad.Models.Produkt> Produkt { get; set; }
        public DbSet<sklad.Models.Objednavka> Objednavka { get; set; }
        public DbSet<sklad.Models.ObjednavkaProdukt> ObjednavkaProdukt { get; set; }
    }
}
