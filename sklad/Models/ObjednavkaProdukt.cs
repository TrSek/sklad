﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sklad.Models
{
    public class ObjednavkaProdukt
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Id_produkt { get; set; }
        [Required]
        public int Id_objednavka { get; set; }
        public ObjednavkaProdukt()
        {

        }
    }
}
