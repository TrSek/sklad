﻿using sklad.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sklad.Models
{
    public class Objednavka
    {
        [Key]
        public int Id { get; set; }
        public string jmeno { get; set; }
        public string adresa { get; set; }
        public DateTime? datum { get; set; }
        public Double cena { get; set; }
        public List<Produkt> produkty;
        public List<Produkt> vsetkyProdukty;

        public Objednavka()
        {
            produkty = new List<Produkt>();
            vsetkyProdukty = new List<Produkt>();
        }

        public override string ToString()
        {
            return Id.ToString()
                + ", Jmeno=" + jmeno
                + ", cena=" + cena
                + ", produkty=" + produkty.Count;
        }

        public async Task UpdateProdukt(ApplicationDbContext _context)
        {
            produkty = new List<Produkt>();
            cena = 0;

            foreach (ObjednavkaProdukt obj in _context.ObjednavkaProdukt.Where(objpr => objpr.Id_objednavka == Id))
            {
                List<Produkt> produktOne = _context.Produkt.Where(pr => pr.Id == obj.Id_produkt).ToList();

                // ma byt len jeden
                if (produktOne.Count != 1)
                {
                    throw new Exception("Chybna polozka produktu. Pocet najdenych v DB pre Id=" + Id + " je " + produktOne.Count + ".");
                }

                produkty.Add(produktOne[0]);
                cena += produktOne[0].cena;
            }
        }

        public async Task UpdateVsetkyProdukty(ApplicationDbContext _context)
        {
            vsetkyProdukty = _context.Produkt.ToList();
        }
    }
}
