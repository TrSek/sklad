﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sklad.Models
{
    public class Produkt
    {
        [Key]
        public int Id { get; set; }
        public string nazev { get; set; }
        public double cena { get; set; }

        public Produkt()
        {

        }

        public override string ToString()
        {
            return Id.ToString()
                + ", nazev=" + nazev
                + ", cena=" + cena;
        }
    }
}
